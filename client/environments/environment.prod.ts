export const environment = {
  production: true,
  metamodelUrl: 'http://crowd.fi.uncoma.edu.ar:3334/',
  metamodelOwlUrl: 'http://crowd.fi.uncoma.edu.ar/crowd2/reasoning/translate/metamodel2owl.php',
  reasoningUrl: 'http://crowd.fi.uncoma.edu.ar/crowd2/reasoning/querying/metamodelsat.php'
};
